import { Component, OnInit } from "@angular/core";
import { IHero } from "./hero";

@Component({
    selector:'hero-root',
    templateUrl:'./hero-list.component.html',
    styleUrls:['./hero-list.component.css']

})
export class HeroListComponent implements OnInit
{
pageTitle: string ='Hero List';
imagewidth:number=50;
imageMargin:number=2;
showImage: boolean=false;
listFilter: string ='Shroff';
message:string;
heroes:IHero[]=[
    {
        "heroId":2,
        "heroName":"Tagore Shroff",
        "heroAge":25,
        "heroMovie":10,
        "starRating":4.2,
        "imageUrl":"http://images.mid-day.com/images/2017/jul/Tiger-Shroff-a.jpg"
    },
    {
        "heroId":1,
        "heroName":"Varun Dhawal",
        "heroAge":27,
        "heroMovie":15,
        "starRating":4.8,
        "imageUrl":"https://images.inuth.com/2017/05/1varundhawanhotwallpaper.jpg"
    },
    {
        "heroId":3,
        "heroName":"Sahid Kapoor",
        "heroAge":32,
        "heroMovie":25,
        "starRating":4.6,
        "imageUrl":" "
    },
    {
        "heroId":4,
        "heroName":"Tushar Kapoor",
        "heroAge":30,
        "heroMovie":18,
        "starRating":5,
        "imageUrl":""
    },
            {
            "heroId":6,
            "heroName":"Aamir khan",
            "heroAge":45,
            "heroMovie":35,
            "starRating":5,
            "imageUrl":"http://worldofpicsandwallpaper.blogspot.in/2013/05/aamir-khan.html"
        },
        {
            "heroId":7,
            "heroName":"Sharukh khan",
            "heroAge":47,
            "heroMovie":45,
            "starRating":5,
            "imageUrl":"http://images6.fanpop.com/image/photos/32700000/SRK-THE-KING-KHAN-shahrukh-khan-32758195-500-469.jpg"
        },
        {
            "heroId":5,
            "heroName":"Salman khan",
            "heroAge":55,
            "heroMovie":50,
            "starRating":5,
            "imageUrl":"https://www.indiatoday.in/movies/celebrities/story/salman-khan-single-status-bachelorhood-iulia-vantur-1176056-2018-02-23"
        },
        {
            "heroId":8,
            "heroName":"Sidharth Malhotra",
            "heroAge":29,
            "heroMovie":8,
            "starRating":4,
            "imageUrl":"https://www.pinterest.com/pin/208924870193300015/"
        },
        {
            "heroId":9,
            "heroName":"Ranvir Kapoor",
            "heroAge":28,
            "heroMovie":12,
            "starRating":5,
            "imageUrl":"http://www.santabanta.com/images/ranbir-kapoor/13588/"
        },
        {
            "heroId":10,
            "heroName":"Sushant Singh Rajput",
            "heroAge":32,
            "heroMovie":9,
            "starRating":4.2,
            "imageUrl":"https://timesofindia.indiatimes.com/entertainment/hindi/bollywood/photo-features/bollywood-stars-who-refused-to-endorse-fairness-creams/Sushant-Singh-Rajput-turns-down-Rs-15-crore-fairness-cream-endorsement-deal/photostory/62469397.cms"
        },
];
toggleImage(): void{
    this.showImage =!this.showImage
}
ngOnInit(): void{
    console.log('In OnInit')
}
}